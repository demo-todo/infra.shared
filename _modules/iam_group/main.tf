resource "aws_iam_group" "this" {
  name = var.name
}

resource "aws_iam_group_policy_attachment" "arn" {
  count = length(var.managed_policy_arns)

  group      = aws_iam_group.this.name
  policy_arn = var.managed_policy_arns[count.index]
}

data "local_file" "policy-doc" {
  count = length(var.customer_policy_files)

  filename = var.customer_policy_files[count.index]
}

resource "aws_iam_policy" "policy" {
  count = length(data.local_file.policy-doc)

  name = trimsuffix(
    reverse(split("/", data.local_file.policy-doc[count.index].filename))[0], ".json"
  )
  path   = "/"
  policy = data.local_file.policy-doc[count.index].content
}

resource "aws_iam_group_policy_attachment" "group-policy-attachment" {
  count = length(aws_iam_policy.policy)

  group      = aws_iam_group.this.name
  policy_arn = aws_iam_policy.policy[count.index].arn
}
