variable "name" {
  type = string
}

variable "managed_policy_arns" {
  type    = list(string)
  default = []
}

variable "customer_policy_files" {
  type    = list(string)
  default = []
}
