resource "aws_iam_user" "this" {
  name = var.username

  tags = merge(var.tags, map("terraform", true))
}

resource "aws_iam_user_group_membership" "this" {
  user = aws_iam_user.this.name

  groups = var.groups
}
