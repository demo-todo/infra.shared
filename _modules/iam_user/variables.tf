variable "username" {
  type = string
}

variable "tags" {
  type = map
  default = {}
}

variable "groups" {
  type = list(string)
}
