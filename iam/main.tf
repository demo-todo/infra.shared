module "iam_group_administrators" {
  source = "../_modules/iam_group"

  name = "administrators"
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/AdministratorAccess"
  ]
}

module "iam_group_developers" {
  source = "../_modules/iam_group"

  name = "developers"
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/CloudWatchLogsReadOnlyAccess",
    "arn:aws:iam::aws:policy/IAMUserChangePassword"
  ]
  customer_policy_files = [
    "files/AllowIAMCredentialManagement.json",
    "files/AllowIAMRead.json"
  ]
}

module "user_admin" {
  source = "../_modules/iam_user"

  username = "admin@demo.com"
  groups = [
    module.iam_group_administrators.group_name
  ]
}

module "user_dev" {
  source = "../_modules/iam_user"

  username = "dev@demo.com"
  groups = [
    module.iam_group_developers.group_name
  ]
}
