terraform {
  backend "s3" {
    bucket         = "demo-todo-state"
    dynamodb_table = "demo-todo-tf-lock"
    encrypt        = true
    region         = "eu-central-1"
    key            = "infra/shared/terraform.tfstate"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.22.0"
    }
  }
}

provider "aws" {
  region = var.region
}
