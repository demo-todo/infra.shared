variable "region" {
  description = "Default region where infrastructure will be deployed"
  type        = string
}

variable "name" {
  description = "Name of the project"
  type        = string
}

variable "domain_name" {
  type = string
}

variable "tags" {
  type    = map(any)
  default = {}
}
